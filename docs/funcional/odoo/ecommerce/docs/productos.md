## Crear productos con características

![type:video](../videos/crearproductos.mp4)

## Crear productos con variantes

![type:video](../videos/crearproductosvar.mp4)

## Cantidades en stock

![type:video](../videos/cantidades.mp4)
