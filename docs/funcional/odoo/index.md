# Odoo

## Comercio electrónico

- [Categorías del producto](ecommerce/docs/categorias.md)
- [Atributos](ecommerce/docs/atributos.md)
- [Etiquetas de producto](ecommerce/docs/etiquetas.md)
- [Productos](ecommerce/docs/productos.md)
